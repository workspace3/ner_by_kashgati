# -*- coding: utf-8 -*-
"""
"""
import os
import sys
from conf.config import get_path

project_path = os.path.normpath(os.path.join(os.path.dirname(__file__),
                                             "../../"))


def _load(file_name):
    with open(file_name, encoding="utf8")as fi:
        data_x, data_y, curr_x, curr_y = [], [], [], []
        for line in fi.readlines():
            if "\n" == line:
                data_x.append(curr_x)
                data_y.append(curr_y)
                curr_x = []
                curr_y = []
            else:
                parts = line.strip().split(" ")
                if len(parts) != 2:
                    print("数据错误：{}".format(line), file=sys.stderr)
                else:
                    curr_x.append(parts[0])
                    curr_y.append(parts[1])
    return data_x, data_y


class DataLoader:
    def __init__(self):
        paras = get_path()
        self.train_file = os.path.join(project_path, paras["train"])
        self.dev_file = os.path.join(project_path, paras["dev"])
        self.test_file = os.path.join(project_path, paras["dev"])

    def load_train(self):
        return _load(self.train_file)

    def load_dev(self):
        return _load(self.dev_file)

    def load_test(self):
        return _load(self.test_file)


if __name__ == "__main__":
    data_loader = DataLoader()
    train = data_loader.load_train()
    print(len(train[0][0]), train[0][0])
    print(train[1][0])
    dev = data_loader.load_dev()
    print(len(dev[0][0]), dev[0][0])
    print(dev[1][0])
