# -*- coding: utf-8 -*-
"""
"""
import os
import sys
import kashgari
from kashgari.embeddings import BERTEmbedding
from kashgari.tasks.labeling import BiGRU_CRF_Model

project_path = os.path.normpath(os.path.join(os.path.dirname(__file__),
                                             "../../"))

sys.path.insert(0, project_path)

from conf.config import get_bert_model_path
from conf.config import get_path
paths = get_path()


class NerModel:
    def __init__(self):
        self.model_save_path = os.path.normpath(os.path.join(project_path, paths["model_save_path"]))
        if os.path.exists(self.model_save_path):
            self.model = kashgari.utils.load_model(model_path=self.model_save_path)
        else:
            bert_model_path = os.path.join(project_path, get_bert_model_path())
            bert_embedding = BERTEmbedding(model_folder=bert_model_path,
                                           task=kashgari.LABELING,
                                           sequence_length=16)
            self.model = BiGRU_CRF_Model(embedding=bert_embedding)

    def train(self, train_x, train_y, dev_x, dev_y):
        self.model.fit(train_x, train_y, dev_x, dev_y, batch_size=8, epochs=5)
        self.model.save(model_path=self.model_save_path)

    def predict(self, sentence):
        test_x = [list(sentence)]
        result = self.model.predict(test_x, batch_size=1)
        return result

    def test(self, test_x, test_y):
        result = self.model.evaluate(test_x, test_y)
        return result


def _test():
    from pprint import pprint
    from src.handler.data_load import DataLoader
    data_loader = DataLoader()
    train_x, train_y = data_loader.load_train()
    dev_x, dev_y = data_loader.load_dev()
    test_x, test_y = dev_x[:10], dev_y[:10]
    ner_model = NerModel()
    ner_model.train(train_x, train_y, dev_x, dev_y)
    result = ner_model.test(test_x, test_y)
    pprint(result)
    sent = input("请输入：")
    while "q" != sent:
        result = ner_model.predict(sent)[0]
        sent_copy = "".join([sent[i] if "O" != result[i] else " " for i in range(len(result))])
        keywords = [item for item in sent_copy.split() if len(item) > 0]
        print("抽取结果：{}".format(", ".join(keywords)))
        sent = input("请输入：")


if __name__ == "__main__":
    _test()
