# -*- coding: utf-8 -*-
import os
from configparser import ConfigParser

project_path = os.path.normpath(os.path.join(os.path.dirname(__file__),
                                             "../"))


def get_bert_model_path():
    config_file = os.path.normpath(os.path.join(project_path, "conf/const_conf.ini"))
    conf = ConfigParser()
    conf.read(config_file)
    model_dir = conf.get("bert_model_path", "bert_model_dir")
    return model_dir


def get_path():
    config_file = os.path.normpath(os.path.join(project_path, "conf/const_conf.ini"))
    conf = ConfigParser()
    conf.read(config_file)
    paras = dict()
    train_data = conf.get("path", "train_data")
    dev_data = conf.get("path", "dev_data")
    model_save_path = conf.get("path", "model_save_path")
    paras["train"] = train_data
    paras["dev"] = dev_data
    paras["model_save_path"] = model_save_path
    return paras
